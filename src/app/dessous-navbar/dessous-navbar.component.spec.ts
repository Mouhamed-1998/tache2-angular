import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DessousNavbarComponent } from './dessous-navbar.component';

describe('DessousNavbarComponent', () => {
  let component: DessousNavbarComponent;
  let fixture: ComponentFixture<DessousNavbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DessousNavbarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DessousNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
