import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { DessousNavbarComponent } from './dessous-navbar/dessous-navbar.component';
import { HealthyComponent } from './healthy/healthy.component';
import { ClassesComponent } from './classes/classes.component';
import { FitnessComponent } from './fitness/fitness.component';
import { CountComponent } from './count/count.component';
import { SchuduleComponent } from './schudule/schudule.component';
import { PartenerComponent } from './partener/partener.component';
import { TrainersComponent } from './trainers/trainers.component';
import { ServicesComponent } from './services/services.component';
import { FooterComponent } from './footer/footer.component';
import { FormeComponent } from './forme/forme.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DessousNavbarComponent,
    HealthyComponent,
    ClassesComponent,
    FitnessComponent,
    CountComponent,
    SchuduleComponent,
    PartenerComponent,
    TrainersComponent,
    ServicesComponent,
    FooterComponent,
    FormeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
